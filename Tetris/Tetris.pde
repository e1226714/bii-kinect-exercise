/*
  Tetris
  Author: Karl Hiner
  Controls:
  LEFT/RIGHT/DOWN to move
  UP - flip
  SPACE - hard drop (drop immediately)
*/

//import controlP5.*;
import SimpleOpenNI.*;

color[]       userClr = new color[]{ color(255,0,0),
                                     color(0,255,0),
                                     color(0,0,255),
                                     color(255,255,0),
                                     color(255,0,255),
                                     color(0,255,255)
                                   };
                                   
PVector com = new PVector();                                   
PVector com2d = new PVector();    


final int CYAN = color(0,255,255);
final int ORANGE = color(255,165,0);
final int YELLOW = color(255,255,0);
final int PURPLE = color(160,32,240);
final int BLUE = color(0,0,255);
final int RED = color(255,0,0);
final int GREEN = color(0,255,0);


//-------
SimpleOpenNI context;
//-------

//int[] userList = context.getUsers();

final int yValue = 1250;

//ControlP5 controlP5;
Grid board, preview;
Tetromino curr;
Shape next;
Shape[] shapes = new Shape[7];
int timer = 20;
int currTime = 0;
int score = 0;
int lines = 0;
int level = 1;
final int SPEED_DECREASE = 2;
public static boolean game_over = false;

boolean existing_user = false;
int lastUserPosition = 0;
boolean rightHandUp = true;
boolean leftKneeUp = true;
boolean leftHandDown = false;

//kinect to tetris conversion
int[] gameFields = new int[10];

void setup() {
  size(1920, 1080, OPENGL);
  
  drawBackground();
  //------------
    context = new SimpleOpenNI(this);
  if(context.isInit() == false)
  {
     println("Can't init SimpleOpenNI, maybe the camera is not connected!"); 
     exit();
     return;  
  }
  
  
  context.enableDepth();
  context.enableRGB();
  context.enableUser();
  context.setMirror(true);
  
  // -----------------
  
  
  textSize(35);
  //controlP5 = new ControlP5(this);
  //controlP5.addButton("play", 1, width/2 - 35, height/2, 70, 20).setLabel("play again");
  shapes[0] = new Shape(4, new int[] {8,9,10,11}, CYAN);  // I
  shapes[1] = new Shape(3, new int[] {0,3,4,5}, BLUE);  // J
  shapes[2] = new Shape(3, new int[] {2,3,4,5}, ORANGE);  // L
  shapes[3] = new Shape(2, new int[] {0,1,2,3}, YELLOW);  // O
  shapes[4] = new Shape(4, new int[] {5,6,8,9}, GREEN);  // S
  shapes[5] = new Shape(3, new int[] {1,3,4,5,}, PURPLE);  // T
  shapes[6] = new Shape(4, new int[] {4,5,9,10}, RED);  // Z
  board = new Grid(710, 20, 500, 1000, 20, 10); // ingame grid
  preview = new Grid(yValue, 20, 200, 100, 2, 4); // changes top right box
  next = shapes[(int)random(7)];
 // loadNext();
}

void draw() {
  drawBackground();
  
  //KINECT LOOP
  drawKinect();
  
  //TETRIS LOOP
  drawTetris();

}

void drawKinect(){
  stroke(255);
    strokeWeight(5);
    rect(30, 280, 640, 480);
  context.update();
  image(context.rgbImage(),30,280);
  
  int[] userList = context.getUsers();
  /*
  for(int i=0;i<userList.length;i++)
  {
    
    if(context.isTrackingSkeleton(userList[i]))
    {
      stroke(userClr[ (userList[i] - 1) % userClr.length ] );
      drawSkeleton(userList[i]);
    }    
    
    // draw the center of mass
    if(context.getCoM(userList[i],com))
    {
      context.convertRealWorldToProjective(com,com2d);
      stroke(100,255,0);
      strokeWeight(1);
      beginShape(LINES);
        vertex(com2d.x,com2d.y - 5);
        vertex(com2d.x,com2d.y + 5);

        vertex(com2d.x - 5,com2d.y);
        vertex(com2d.x + 5,com2d.y);
      endShape();
    
      fill(0,255,100);
      text(Integer.toString(userList[i]),com2d.x,com2d.y);
    
    }
  }
  */
}

void drawTetris(){
  
  if (game_over) {
    text("GAME OVER\nSCORE: " + score, width/2 - 70, height/2 - 50);
    //controlP5.draw(); // show the play again button
    gameOverControlls();
    return;
  }
  if(existing_user){
    kinectControlls();
    currTime++;
    if (currTime >= timer && board.animateCount == -1)
      curr.stepDown();
    preview.draw();
    board.draw();
    if (curr != null)
      curr.draw();
    next.preview();

    fill(255);
  
    text("LEVEL\n" + level, yValue, 200);
    text("LINES\n" + lines, yValue, 320);
    text("SCORE\n" + score, yValue, 440);
  } else {
    text("ENTER THE PLAYGROUND", 800, 520);
  }
}

void gameOverControlls(){
  //println("gamooverController");
  int[] userList = context.getUsers();
  PVector leftHand = new PVector();
  context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_LEFT_HAND,leftHand);
  PVector rightHand = new PVector();
  context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_RIGHT_HAND,rightHand);

  //println("game_over: " + game_over);
  if(game_over && leftHand.y >400 && rightHand.y > 400){
    println("new_game");
    //drawBackground();
    //board.clear();
    //loadNext();
    play(1);
  }
}

void kinectControlls(){
 int[] userList = context.getUsers();
 //move stone right left
 PVector jointPos = new PVector();
 context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_HEAD,jointPos);
  if (lastUserPosition > convertKinectToTetrisPosition(jointPos.x)){
    curr.right();
    lastUserPosition--;
  }
  else if (lastUserPosition < convertKinectToTetrisPosition(jointPos.x)){
    curr.left();
    lastUserPosition++;
  } else if (convertKinectToTetrisPosition(jointPos.x) == 9 && lastUserPosition == 9) {
    curr.left();
  } else if (convertKinectToTetrisPosition(jointPos.x) == 0 && lastUserPosition == 0) {
    curr.right();
  }
  
  //rotate stone
  PVector leftHand = new PVector();
  context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_LEFT_HAND,leftHand);
  if(leftHandDown && leftHand.y >= 150 ){
    curr.rotate();
    leftHandDown = false;
  }
  if(leftHand.y < 150){
    leftHandDown = true;
  }
  
  // SOFT DOWN
  PVector rightHand = new PVector();
  context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_RIGHT_HAND,rightHand);
  if(rightHandUp && rightHand.y < 150 ){
    curr.down();
    //curr.stepDown();
    rightHandUp = false;
  }
  if(rightHand.y >= 150){
    rightHandUp = true;
  }
  
  // HARD DOWN
  PVector leftKnee = new PVector();
  context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_LEFT_KNEE,leftKnee);
  if(leftKneeUp && leftKnee.y < -200 ){
    curr.hardDown();
    leftKneeUp = false;
  }
  if(leftKnee.y >= -100){
    leftKneeUp = true;
  }
  
  
  
}

void loadNext() {
int[] userList = context.getUsers();
 PVector jointPos = new PVector();
 context.getJointPositionSkeleton(userList[0],SimpleOpenNI.SKEL_HEAD,jointPos);
 curr = new Tetromino(next, convertKinectToTetrisPosition(9-jointPos.x));

  next = shapes[(int)random(7)];
  currTime = 0;
  
}

void keyPressed() {
  if (curr == null || game_over)
    return;
  switch(keyCode) {
    case LEFT : curr.left(); break;
    case RIGHT : curr.right(); break;
    case UP : curr.rotate(); break;
    case DOWN : curr.down(); break;
    case ' ' : curr.hardDown(); break;
  }
}

void play(int value) {
  game_over = false;
  score = 0;
  drawBackground();
  board.clear();
  loadNext();
  
}

// draw the skeleton with the selected joints
void drawSkeleton(int userId)
{
  // to get the 3d joint data
  /*
  PVector jointPos = new PVector();
  context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
  println(jointPos);
  */
  
  context.drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);

  context.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);

  context.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);

  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);

  context.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);

  context.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
 
}


void onNewUser(SimpleOpenNI curContext, int userId)
{
  println("onNewUser - userId: " + userId);
  println("\tstart tracking skeleton");
  
  curContext.startTrackingSkeleton(userId);
  if(userId == 1) existing_user = true;
  loadNext();
}

void onLostUser(SimpleOpenNI curContext, int userId)
{
  println("onLostUser - userId: " + userId);
  if(userId == 1) existing_user = false;
}

void onVisibleUser(SimpleOpenNI curContext, int userId)
{
  //println("onVisibleUser - userId: " + userId);
}


//kinect to tetris conversion
int convertKinectToTetrisPosition(float xKinect) {
  if(xKinect < -400){
    return 0;
  } else if(xKinect < -300){
    return 1;
  } else if(xKinect < -200){
    return 2;
  }else if(xKinect < -100){
    return 3;
  }else if(xKinect < 0){
    return 4;
  }else if(xKinect < 100){
    return 5;
  }else if(xKinect < 200){
    return 6;
  }else if(xKinect < 300){
    return 7;
  }else if(xKinect < 400){
    return 8;
  }else if(xKinect >= 400){
    return 9;
  }
  return -1;
}

void drawBackground() {
  background(10);
}

